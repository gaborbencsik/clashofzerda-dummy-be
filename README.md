# Clash of Raptors [dummy backend]

You'd better install it in a virtualenv.

``` bash
# install requirements
pip install -f requirements.txt

# go for it
hug -f clashapi.py
```