import hug
import random
from falcon import HTTP_400, HTTP_401, HTTP_404, HTTP_409

USERS = [
    {
        'id': 0,
        'username': 'cica',
        'password': 'alma',
        'kingdom': 'Cicaland',
        'points': 123
    }
]

BUILDINGS = {
    0: [
        {
            'id': 0,
            'type': 'townhall',
            'level': 2,
            'hp': 100
        },
        {
            'id': 1,
            'type': 'farm',
            'level': 1,
            'hp': 100
        }
    ]
}

RESOURCES = {
    0: [
        {
            'type': 'food',
            'amount': 123
        },
        {
            'type': 'gold',
            'amount': 210
        }
    ]
}

TROOPS = {
    0: [
        {
            'id': 0,
            'level': 1,
            'hp': 100,
            'attack': 10,
            'defence': 10
        },
        {
            'id': 1,
            'level': 2,
            'hp': 150,
            'attack': 20,
            'defence': 10
        }
    ]
}

BATTLES = [
    {
        'id': 0,
        'attacker': {
            'user': USERS[0],
            'troops': TROOPS[0],
            'damage': 100,
            'troopsLost': TROOPS[0]
        },
        'defender': {
            'user': USERS[0],
            'troops': TROOPS[0],
            'damage': 100,
            'troopsLost': TROOPS[0],
            'buildingsLost': BUILDINGS[0]
        }
    }
]

class Datastore():

    def __init__(self, users=None, buildings=None, resources=None, troops=None, battles=None):
        self.users = users if users else []
        self.buildings = buildings if buildings else {}
        self.resources = resources if resources else {}
        self.troops = troops if troops else {}
        self.battles = battles if battles else []

    def search_kingdoms(self, q = None):
        kingdoms = [self.get_kingdom_by_id(user['id']) for user in self.users if not q or q in user['username'] or q in user['kingdom']]

        return kingdoms

    def leaderboard(self):
        kingdoms = sorted(self.users, key = lambda u: u['points'])[::-1]

        return kingdoms

    def add_user(self, username, password, kingdom):
        # check if username already taken
        if len(list(filter(lambda u: u['username'] == username, self.users))):
            return {"errors": {"username": "Username taken, please choose an other one."}}

        # calculate new id
        user_id = sorted(self.users, key=lambda x: x['id'])[-1]['id'] + 1 \
            if len(self.users) else 0

        user = {
            'id': user_id,
            'username': username,
            'password': password,
            'kingdom': kingdom,
            'points': random.randrange(start=10, stop=200)
        }

        self.users.append(user)
        self.buildings[user_id] = [
            {
                'id': 0,
                'type': 'townhall',
                'level': 1,
                'hp': 100
            }
        ];
        self.resources[user_id] = [
            {
                'type': 'food',
                'amount': random.randrange(start=10, stop=50)
            },
            {
                'type': 'gold',
                'amount': random.randrange(start=100, stop=1000)
            }
        ]

        return user

    def add_building(self, user_id, type):
        buildings = self.buildings.get(user_id)

        if buildings:
            id = sorted(buildings, key=lambda x: x['id'])[-1]['id'] + 1 \
                if len(buildings) else 0

            if id > 10 and random.random() < .3:
                return {'errors': {'upgrade': 'Not enough gold.'}}

            building = {
                'id': id,
                'type': type,
                'level': 1,
                'hp': 100
            }

            buildings.append(building)

            return building

        return None

    def upgrade_building(self, user_id, building_id, level):
        buildings = self.buildings.get(user_id)

        if buildings:
            buildings = list(filter(lambda b: b['id'] == building_id, buildings))

            if len(buildings):
                building = buildings[0]

                if level > 3 and random.random() < .3 or level > 10:
                    return {'errors': {'upgrade': 'Not enough gold.'}}

                if building['level'] + 1 == level:
                    building['level'] = level

                    return building
                else:
                    return {'errors': {'level': 'Incorrect level: {}'.format(level)}}

        return None

    def add_troop(self, user_id):
        troops = self.troops.get(user_id)

        if troops:
            id = sorted(troops, key=lambda x: x['id'])[-1]['id'] + 1 \
                if len(troops) else 0

            if id > 10 and random.random() < .3:
                return {'errors': {'upgrade': 'Not enough gold.'}}

            troop = {
                'id': id,
                'level': 1,
                'hp': 100,
                'attack': 10,
                'defence': 10
            }

            troops.append(troop)

            return troop

        return None

    def upgrade_troop(self, user_id, troop_id, level):
        troops = self.troops.get(user_id)

        if troops:
            troops = list(filter(lambda t: t['id'] == troop_id, troops))

            if len(troops):
                troop = troops[0]

                if level > 3 and random.random() < .3 or level > 10:
                    return {'errors': {'upgrade': 'Not enough gold.'}}

                if troop['level'] + 1 == level:
                    troop['level'] = level
                    if random.random() > .5:
                        troop['attack'] += 10
                    else:
                        troop['defence'] += 10

                    return troop
                else:
                    return {'errors': {'level': 'Incorrect level: {}'.format(level)}}

        return None


    def get_user_by_id(self, id):
        users = list(filter(lambda u: u['id'] == id, self.users))

        if len(users):
            return users[0]

        return None

    def get_user_by_username(self, username):
        users = list(filter(lambda u: u['username'] == username, self.users))

        if len(users):
            return users[0]

        return None

    def get_kingdom_by_id(self, id):
        users = list(filter(lambda u: u['id'] == id, self.users))

        if not len(users):
            return None

        user = users[0]
        buildings = self.get_buildings_by_user(id)
        resources = self.get_resources_by_user(id)
        troops = self.get_troops_by_user(id)

        return {
            "user": user,
            "buildings": buildings if buildings else [],
            "resources": resources if resources else [],
            "troops": troops if troops else []
        }

    def get_buildings_by_user(self, user_id):
        buildings = self.buildings.get(user_id)

        return buildings if buildings else []

    def get_resources_by_user(self, user_id):
        resources = self.resources.get(user_id)

        return resources

    def get_troops_by_user(self, user_id):
        troops = self.troops.get(user_id)

        return troops if troops else []

    def get_battles(self, user_id):
        battles = list(filter(lambda b: b['attacker']['user']['id'] == user_id or b['defender']['user']['id'] == user_id, self.battles))

        return battles

    def get_battle(self, user_id, battle_id):
        battle_by_id = list(filter(lambda b: b['id'] == battle_id, self.battles))

        return battle_by_id[0] if len(battle_by_id) else None

    def attack(self, user_id, opponent_id, troop_ids = None):
        attacker = self.get_kingdom_by_id(user_id)
        defender = self.get_kingdom_by_id(opponent_id)
        troops = attacker['troops']

        id = sorted(self.battles, key=lambda x: x['id'])[-1]['id'] + 1 \
            if len(self.battles) else 0

        if troop_ids:
            troops = list(filter(lambda t: t['id'] in [int(t) for t in troop_ids], troops))

        if not attacker:
            return {'errors': {'attacker': 'User not found.'}}

        if not defender:
            return {'errors': {'defenders': 'User not found.'}}

        battle = {
            'id': id,
            'attacker': {
                'user': attacker['user'],
                'troops': troops,
                'damage': random.randrange(start=100, stop=300),
                'troopsLost': list(filter(lambda t: random.random() < .5, troops))
            },
            'defender': {
                'user': defender['user'],
                'troops': defender['troops'],
                'damage': random.randrange(start=100, stop=300),
                'troopsLost': list(filter(lambda t: random.random() < .5, defender['troops'])),
                'buildingsLost': list(filter(lambda b: random.random() < .3, defender['buildings'])),
            }
        }

        self.battles.append(battle)

        return battle


datastore = Datastore(USERS, BUILDINGS, RESOURCES, TROOPS, BATTLES)


@hug.post()
@hug.local()
def register(username: hug.types.text, password: hug.types.text, kingdom: hug.types.text = '', response = None):
    """user registration"""

    kingdom = kingdom if kingdom else "{}'s kingdom".format(username)

    resp = datastore.add_user(username, password, kingdom)

    if 'errors' in resp:
        response.status = HTTP_409

    return resp

@hug.post()
@hug.local()
def login(username: hug.types.text, password: hug.types.text, response = None):
    """user login"""

    user = datastore.get_user_by_username(username)

    if not user:
        response.status = HTTP_401
        return {"errors": {"username": "User does not exist."}}

    if user['password'] != password:
        response.status = HTTP_401
        return {"errors": {"password": "Wrong password."}}

    return user

@hug.get('/kingdom/{user_id}')
@hug.local()
def kingdom(user_id: hug.types.number, response = None):
    kingdom = datastore.get_kingdom_by_id(user_id)

    if not kingdom:
        response.status = HTTP_404
        return {"errors": {"kingdom": "User id not found."}}

    return kingdom

@hug.get('/kingdom/{user_id}/buildings')
@hug.local()
def buildings(user_id: hug.types.number, response = None):
    kingdom = datastore.get_kingdom_by_id(user_id)

    if not kingdom:
        response.status = HTTP_404
        return {"errors": {"kingdom": "User id not found."}}

    return kingdom.get('buildings')

@hug.get('/kingdom/{user_id}/buildings/{building_id}')
@hug.local()
def building(user_id: hug.types.number, building_id: hug.types.number, response = None):
    kingdom = datastore.get_kingdom_by_id(user_id)

    if not kingdom:
        response.status = HTTP_404
        return {"errors": {"kingdom": "User id not found."}}

    buildings = kingdom.get('buildings')
    buildings = buildings if buildings else []
    buildings = list(filter(lambda b: b['id'] == building_id, buildings))

    if not len(buildings):
        response.status = HTTP_404
        return {"errors": {"building": "Building not found."}}

    return buildings[0]

@hug.get('/kingdom/{user_id}/resources')
@hug.local()
def resources(user_id: hug.types.number, response = None):
    kingdom = datastore.get_kingdom_by_id(user_id)

    if not kingdom:
        response.status = HTTP_404
        return {"errors": {"kingdom": "User id not found."}}

    return kingdom.get('resources')

@hug.get('/kingdom/{user_id}/resources/{resource_type}')
@hug.local()
def resource(user_id: hug.types.number, resource_type: hug.types.text, response = None):
    kingdom = datastore.get_kingdom_by_id(user_id)

    if not kingdom:
        response.status = HTTP_404
        return {"errors": {"kingdom": "User id not found."}}

    resources = kingdom.get('resources')
    resources = resources if resources else []
    resources = list(filter(lambda r: r['type'] == resource_type, resources))

    if not len(resources):
        response.status = HTTP_404
        return {"errors": {"resource": "Resource not found."}}

    return resources[0]

@hug.get('/kingdom/{user_id}/troops')
@hug.local()
def troops(user_id: hug.types.number, response = None):
    kingdom = datastore.get_kingdom_by_id(user_id)

    if not kingdom:
        response.status = HTTP_404
        return {"errors": {"kingdom": "User id not found."}}

    return kingdom.get('troops')

@hug.get('/kingdom/{user_id}/troops/{troop_id}')
@hug.local()
def troop(user_id: hug.types.number, troop_id: hug.types.number, response = None):
    kingdom = datastore.get_kingdom_by_id(user_id)

    if not kingdom:
        response.status = HTTP_404
        return {"errors": {"kingdom": "User id not found."}}

    troops = kingdom.get('troops')
    troops = troops if troops else []
    troops = list(filter(lambda t: t['id'] == troop_id, troops))

    if not len(troops):
        response.status = HTTP_404
        return {"errors": {"troop": "Troop not found."}}

    return troops[0]

@hug.post('/kingdom/{user_id}/buildings')
@hug.local()
def add_building(user_id: hug.types.number, type: hug.types.text = None, response = None):
    kingdom = datastore.get_kingdom_by_id(user_id)

    if not kingdom:
        response.status = HTTP_404
        return {"errors": {"kingdom": "User id not found."}}

    if type not in ['farm', 'mine', 'barracks']:
        response.status = HTTP_400
        return {"errors": {"type": "Invalid type: {}".format(type)}}

    building = datastore.add_building(user_id, type)

    if not building:
        response.status = HTTP_400
        return {"errors": {"unknown": "Error creating building"}}

    if 'errors' in building:
        response.status = HTTP_400

    return building

@hug.post('/kingdom/{user_id}/troops')
@hug.local()
def add_troop(user_id: hug.types.number, response = None):
    kingdom = datastore.get_kingdom_by_id(user_id)

    if not kingdom:
        response.status = HTTP_404
        return {"errors": {"kingdom": "User id not found."}}

    troop = datastore.add_troop(user_id)

    if not troop:
        response.status = HTTP_400
        return {"errors": {"unknown": "Error creating troop"}}

    if 'errors' in troop:
        response.status = HTTP_400

    return troop

@hug.put('/kingdom/{user_id}/buildings/{building_id}')
@hug.local()
def upgrade_building(user_id: hug.types.number, building_id: hug.types.number, level: hug.types.number = 0, response = None):
    kingdom = datastore.get_kingdom_by_id(user_id)

    if not kingdom:
        response.status = HTTP_404
        return {"errors": {"kingdom": "User id not found."}}

    building = datastore.upgrade_building(user_id, building_id, level)

    if not building:
        response.status = HTTP_400
        return {"errors": {"unknown": "Error creating building"}}

    if 'errors' in building:
        response.status = HTTP_400

    return building

@hug.put('/kingdom/{user_id}/troops/{troop_id}')
@hug.local()
def upgrade_troop(user_id: hug.types.number, troop_id: hug.types.number, level: hug.types.number = 0, response = None):
    kingdom = datastore.get_kingdom_by_id(user_id)

    if not kingdom:
        response.status = HTTP_404
        return {"errors": {"kingdom": "User id not found."}}

    troop = datastore.upgrade_troop(user_id, troop_id, level)

    if not troop:
        response.status = HTTP_400
        return {"errors": {"unknown": "Error creating troop"}}

    if 'errors' in troop:
        response.status = HTTP_400

    return troop

@hug.get('/kingdom/{user_id}/battles')
@hug.local()
def battles(user_id: hug.types.number, response = None):
    return datastore.get_battles(user_id)

@hug.get('/kingdom/{user_id}/battles/{battle_id}')
@hug.local()
def battle(user_id: hug.types.number, battle_id: hug.types.number, response = None):
    battle = datastore.get_battle(user_id, battle_id)

    if not battle:
        response.status = HTTP_404
        return {"errors": {"battle": "Battle not found."}}

    return battle

@hug.post('/kingdom/{user_id}/attack/')
@hug.local()
def attack(user_id: hug.types.number, opponent: hug.types.number, troops: hug.types.text = None, response = None):
    troop_ids = troops.split(',') if troops else []

    battle = datastore.attack(user_id, opponent, troop_ids)

    if 'errors' in battle:
        response.status = HTTP_400

    return battle

@hug.get('/search')
@hug.local()
def search(q: hug.types.text = None, response = None):
    return datastore.search_kingdoms(q)

@hug.get('/leaderboard')
@hug.local()
def leaderboard(response = None):
    return datastore.leaderboard()

